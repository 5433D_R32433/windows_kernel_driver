#include <ntddk.h>
#include <wdm.h>
#include <ndis.h>

NDIS_HANDLE NdisFilterDriverHandle = NULL;

FILTER_DETACH               FilterDetach;
FILTER_ATTACH               FilterAttach;
FILTER_RESTART              FilterRestart;
SET_OPTIONS                 FilterSetOptions;
FILTER_PAUSE                FilterPause;
FILTER_OID_REQUEST          FilterOidRequest;
FILTER_OID_REQUEST_COMPLETE FilterOidRequestComplete;

VOID UnloadDriver ( _In_ PDRIVER_OBJECT DriverObject )
{
    UNREFERENCED_PARAMETER(DriverObject);

    if ( NdisFilterDriverHandle != NULL ) 
	{
        NdisFDeregisterFilterDriver ( NdisFilterDriverHandle );
    }

    KdPrint ( ( "Filter driver unloaded.\n" ) );
}

NTSTATUS DriverEntry ( _In_ PDRIVER_OBJECT DriverObject, _In_ PUNICODE_STRING RegistryPath )
{
    UNREFERENCED_PARAMETER ( RegistryPath );

    DriverObject->DriverUnload                                     = UnloadDriver;
    NdisFilterDriverHandle                                         = NULL;
	
    NDIS_FILTER_DRIVER_CHARACTERISTICS filter_characteristics      = { 0 };
    NdisZeroMemory ( &filter_characteristics, sizeof ( NDIS_FILTER_DRIVER_CHARACTERISTICS ) );

    filter_characteristics.Header.Type                             = NDIS_OBJECT_TYPE_FILTER_DRIVER_CHARACTERISTICS;
    filter_characteristics.Header.Size                             = NDIS_SIZEOF_FILTER_DRIVER_CHARACTERISTICS_REVISION_1;
    filter_characteristics.Header.Revision                         = NDIS_FILTER_CHARACTERISTICS_REVISION_1;
    filter_characteristics.MajorNdisVersion                        = 0x6;
    filter_characteristics.MinorNdisVersion                        = 0;
    filter_characteristics.MajorDriverVersion                      = 1;
    filter_characteristics.MinorDriverVersion                      = 0;
    filter_characteristics.Flags                                   = 0;
	
	filter_characteristics.SetOptionsHandler                       = FilterSetOptions;
	filter_characteristics.SetFilterModuleOptionsHandler           = NULL;
	filter_characteristics.AttachHandler                           = FilterAttach;
	filter_characteristics.DetachHandler                           = FilterDetach;
	filter_characteristics.RestartHandler                          = FilterRestart;
	filter_characteristics.PauseHandler                            = FilterPause;
	filter_characteristics.SendNetBufferListsHandler               = NULL;
	filter_characteristics.SendNetBufferListsCompleteHandler       = NULL;
	filter_characteristics.CancelSendNetBufferListsHandler         = NULL;
	filter_characteristics.ReceiveNetBufferListsHandler            = NULL;
	filter_characteristics.ReturnNetBufferListsHandler             = NULL;
	filter_characteristics.OidRequestHandler                       = FilterOidRequest;
	filter_characteristics.OidRequestCompleteHandler               = FilterOidRequestComplete;
	filter_characteristics.CancelOidRequestHandler                 = NULL;
	filter_characteristics.DevicePnPEventNotifyHandler             = NULL;
	filter_characteristics.NetPnPEventHandler                      = NULL;
	filter_characteristics.StatusHandler                           = NULL;
	// filter_characteristics.DirectOidRequestHandler                 = NULL;
	// filter_characteristics.DirectOidRequestCompleteHandler         = NULL;
	// filter_characteristics.CancelDirectOidRequestHandler           = NULL;
	// filter_characteristics.SynchronousOidRequestHandler            = NULL;
	// filter_characteristics.SynchronousOidRequestCompleteHandler    = NULL;

	
    NTSTATUS status = NdisFRegisterFilterDriver ( DriverObject, ( NDIS_HANDLE ) DriverObject, &filter_characteristics, &NdisFilterDriverHandle );
    if ( NT_SUCCESS ( status ) ) 
	{
        KdPrint ( ( "Filter driver loaded successfully.\n" ) );
    } 
	else 
	{
        KdPrint ( ( "Filter driver failed to load with status: 0x%X\n", status ) );
    }

    return status;
}

NDIS_STATUS FilterSetOptions ( _In_ NDIS_HANDLE NdisDriverHandle, _In_ NDIS_HANDLE DriverContext )
{
    UNREFERENCED_PARAMETER ( NdisDriverHandle );
    UNREFERENCED_PARAMETER ( DriverContext    );
    KdPrint(("FilterSetOptions called.\n"));
	
	return NDIS_STATUS_SUCCESS;
}

NDIS_STATUS FilterAttach ( _In_ NDIS_HANDLE NdisFilterHandle, _In_ NDIS_HANDLE FilterDriverContext, _In_ PNDIS_FILTER_ATTACH_PARAMETERS AttachParameters )
{
    UNREFERENCED_PARAMETER ( NdisFilterHandle    );
    UNREFERENCED_PARAMETER ( FilterDriverContext );
    UNREFERENCED_PARAMETER ( AttachParameters    );
    KdPrint ( ( "FilterAttach called.\n" ) );
    return NDIS_STATUS_SUCCESS;
}

VOID FilterDetach ( _In_ NDIS_HANDLE FilterModuleContext )
{
    UNREFERENCED_PARAMETER ( FilterModuleContext );
    KdPrint ( ( "FilterDetach called.\n" ) );
}


NDIS_STATUS FilterPause ( _In_ NDIS_HANDLE FilterModuleContext, _In_ PNDIS_FILTER_PAUSE_PARAMETERS PauseParameters )
{
    UNREFERENCED_PARAMETER ( FilterModuleContext );
    UNREFERENCED_PARAMETER ( PauseParameters     );
    KdPrint ( ( "FilterPause called.\n" ) );
    return NDIS_STATUS_SUCCESS;
}

NDIS_STATUS FilterRestart ( _In_ NDIS_HANDLE FilterModuleContext, _In_ PNDIS_FILTER_RESTART_PARAMETERS RestartParameters )
{
    UNREFERENCED_PARAMETER ( FilterModuleContext );
    UNREFERENCED_PARAMETER ( RestartParameters   );
    KdPrint ( ( "FilterRestart called.\n" ) );
    return NDIS_STATUS_SUCCESS;
}

NDIS_STATUS FilterOidRequest ( _In_ NDIS_HANDLE FilterModuleContext, _In_ PNDIS_OID_REQUEST OidRequest )
{
    UNREFERENCED_PARAMETER ( FilterModuleContext );
    UNREFERENCED_PARAMETER ( OidRequest          );
    KdPrint ( ( "FilterOidRequest called.\n" ) );
    return NDIS_STATUS_SUCCESS;
}

VOID FilterOidRequestComplete ( _In_ NDIS_HANDLE FilterModuleContext, _In_ PNDIS_OID_REQUEST OidRequest, _In_ NDIS_STATUS Status )
{
    UNREFERENCED_PARAMETER ( FilterModuleContext );
    UNREFERENCED_PARAMETER ( OidRequest          );
    KdPrint ( ( "FilterOidRequestComplete called with Status: 0x%X.\n", Status ) );
}


