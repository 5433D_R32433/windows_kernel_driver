#include <windows.h>
#include <stdio.h>

#define DRIVER_PATH "C:\\Users\\1032\\Desktop\\windows_kernel_module\\skeleton\\skeleton.sys"

int _cdecl main ( void )
{
    HANDLE         hSCManager;
    HANDLE         hService;
    SERVICE_STATUS service_status;
    hSCManager = OpenSCManager ( NULL, NULL, SC_MANAGER_CREATE_SERVICE );
    
	printf ( "[+] Load Driver...\n" );

    if ( hSCManager )
    {
        printf ( "[+] Create Service...\n" );

        hService = CreateService ( hSCManager, 
								   "SkeletonDriver", 
                                   "Sekleton Windows NT Driver", 
                                   SERVICE_START | DELETE | SERVICE_STOP, 
                                   SERVICE_KERNEL_DRIVER,
                                   SERVICE_DEMAND_START, 
                                   SERVICE_ERROR_IGNORE, 
                                   DRIVER_PATH, 
                                   NULL, 
								   NULL, 
								   NULL, 
								   NULL, 
								   NULL );
        if ( !hService )
        {
            hService = OpenService ( hSCManager, "SkeletonDriver",  SERVICE_START | DELETE | SERVICE_STOP );
        }

        if ( hService )
        {
            printf ( "Start Service\n" );

            StartService ( hService, 0, NULL );
            printf       ( "Press Enter to close service\r\n" );
            getchar      ( );
            ControlService     ( hService, SERVICE_CONTROL_STOP, &service_status );
            DeleteService      ( hService );
            CloseServiceHandle ( hService );
        }

        CloseServiceHandle ( hSCManager );
    }
    
    return 0;
}