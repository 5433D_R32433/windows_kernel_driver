@echo off

set INCLUDES=-I"C:\\Program Files (x86)\\Windows Kits\\10\\Include\\10.0.22621.0\\km" -I.
set LIBPATH="C:\\Program Files (x86)\\Windows Kits\10\Lib\\10.0.22621.0\\km\\x64"
set LIBS=NtosKrnl.lib hal.lib wmilib.lib ndis.lib

set DEFINES= -D NDIS_SUPPORT_NDIS6 -D "WIN32" -D "_WIN32_WINNT=0x0601" -D "WINNT=1" -D "WIN64" -D "_UNICODE" -D "UNICODE" -D "NTDDI_VERSION=NTDDI_WIN7" -D "_AMD64_" -D "AMD64"  -D "MSC_NOOPT" -D "_M_AMD64" -D "DBG=1" -D "_DEBUG" -D "DEVL" -D "_NT_TARGET_VERSION_WINBLUE_" -D "_NT_TARGET_VERSION_WIN7_" -D "_NT_TARGET_VERSION_WIN8_" -D "_NT_TARGET_VERSION_WINBLUE_" -D "_NT_TARGET_VERSION_WIN10_" -D "_NT_TARGET_VERSION_WIN11_" -D "_NT_TARGET_VERSION=0x0A00" -D "_NT_DRIVER" -D "_KERNEL_MODE_DRIVER" -D "_WIN32" -D "_WIN64" -D "KERNEL_MODE" -D "DRIVER" -D "DBG" 
set CFLAGS=-wd"4324" -wd"4748" -wd"4603" -wd"4627" -wd"4201" -wd"4986" -wd"4987" -Zi -Gm- -Od -GS -W4 -GF -WX -GR- -Gz -Oy- -Oi -FC %DEFINES% 
set LDFLAGS=-link -libpath:%LIBPATH% -OUT:NDISFilterDriver.sys -DRIVER -DEBUG -ENTRY:DriverEntry -WX -SUBSYSTEM:NATIVE -OPT:REF -MACHINE:X64 -INCREMENTAL:NO -MERGE:"_TEXT=.text;_PAGE=PAGE" -NODEFAULTLIB -MANIFEST:NO
set SRCS=ndis_filter.c

cl.exe %INCLUDES% %CFLAGS% %SRCS% %LDFLAGS% %LIBS%

makecert -r -pe -ss PrivateCertStore -n CN=rezaee.net(Test) rezaeetest.cer
signtool sign /fd sha256 /v /s PrivateCertStore /n rezaee.net(Test) /t http://timestamp.digicert.com "NDISFilterDriver.sys"
REM signtool verify  /v  /kp skeleton.sys
certmgr /del /c /s PrivateCertStore
del /Q /S rezaeetest.cer


