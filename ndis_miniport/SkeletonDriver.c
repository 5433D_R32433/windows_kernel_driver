#include <ntddk.h>
#include <wdm.h>

NTSTATUS DriverEntry    ( _In_ PDRIVER_OBJECT   DriverObject, _In_ PUNICODE_STRING  RegistryPath );
VOID DriverUnload       ( _In_ PDRIVER_OBJECT   DriverObject );
NTSTATUS CreateClose    ( _In_ PDEVICE_OBJECT   DeviceObject, _In_ PIRP Irp );
NTSTATUS DriverDispatch ( _In_ PDEVICE_OBJECT   DeviceObject, _In_ PIRP Irp );


NTSTATUS DriverEntry ( _In_ PDRIVER_OBJECT   DriverObject, _In_ PUNICODE_STRING  RegistryPath )
{
    UNREFERENCED_PARAMETER     ( RegistryPath );
    DriverObject->DriverUnload = DriverUnload;

    DriverObject->MajorFunction [ IRP_MJ_CREATE ] = CreateClose;
    DriverObject->MajorFunction [ IRP_MJ_CLOSE  ] = CreateClose;


	for ( ULONG i = 0; i < IRP_MJ_MAXIMUM_FUNCTION; ++i ) 
	{
        DriverObject->MajorFunction [ i ] = DriverDispatch;
    }


    UNICODE_STRING SkeletonSymbolicLinkName;
    RtlInitUnicodeString ( &SkeletonSymbolicLinkName, L"\\DosDevices\\SkeletonDriver");
	
	UNICODE_STRING SkeletonDriverName;
	RtlInitUnicodeString ( &SkeletonDriverName, L"\\Device\\SkeletonDriver" );
    
    PDEVICE_OBJECT DeviceObject;
    NTSTATUS status = IoCreateDevice ( DriverObject,
									   0,
									   &SkeletonDriverName,
									   FILE_DEVICE_UNKNOWN,
									   FILE_DEVICE_SECURE_OPEN,
									   FALSE,
									   &DeviceObject );

    if ( !NT_SUCCESS ( status ) ) 
	{
        KdPrint ( ( "Failed to create the device object (0x%08X)\n", status ) );
        return status;
    }
	
    status = IoCreateSymbolicLink ( &SkeletonSymbolicLinkName, &SkeletonDriverName );

    if ( !NT_SUCCESS ( status ) ) 
	{
        KdPrint        ( ( "Failed to create symbolic link (0x%08X)\n", status ) );
        IoDeleteDevice ( DeviceObject );
        return status;
    }

    KdPrint ( ( "Skeleton Driver Loaded Successfully\n" ) );

    return STATUS_SUCCESS;
}

VOID DriverUnload ( _In_ PDRIVER_OBJECT DriverObject )
{
    UNREFERENCED_PARAMETER ( DriverObject );
    UNICODE_STRING SkeletonSymbolicLinkName;
    RtlInitUnicodeString ( &SkeletonSymbolicLinkName, L"\\DosDevices\\SkeletonDriver" );
    IoDeleteSymbolicLink ( &SkeletonSymbolicLinkName );
	
	IoDeleteDevice ( DriverObject->DeviceObject );

    KdPrint ( ( "Skeleton Driver Unloaded\n" ) );
}

NTSTATUS CreateClose ( _In_ PDEVICE_OBJECT DeviceObject, _In_ PIRP Irp )
{
    UNREFERENCED_PARAMETER ( DeviceObject );
    Irp->IoStatus.Status      = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest ( Irp, IO_NO_INCREMENT );
    return STATUS_SUCCESS;
}

NTSTATUS DriverDispatch ( _In_ PDEVICE_OBJECT DeviceObject, _In_ PIRP Irp )
{
    UNREFERENCED_PARAMETER ( DeviceObject );
	
	KdPrint ( ( "IRP Type 0x%X Handled\n", IoGetCurrentIrpStackLocation ( Irp )->MajorFunction ) );
	
    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest     ( Irp, IO_NO_INCREMENT );
    return STATUS_SUCCESS;
}
