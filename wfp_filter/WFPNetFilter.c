#include <ntddk.h>
// #include <wdm.h>
#include <fwpmk.h>
#include <fwpsk.h>
#define INITGUID
#include <guiddef.h>
#include <fwpmu.h>

NTSTATUS DriverEntry    ( _In_ PDRIVER_OBJECT   DriverObject, _In_ PUNICODE_STRING  RegistryPath );
VOID DriverUnload       ( _In_ PDRIVER_OBJECT   DriverObject );

HANDLE EngineHandle;

DEFINE_GUID ( WFP_SAMPLE_ESTABLISHED_CALLOUT_V4_GUID, 0xd969fc67, 0x6fb2, 0x4504, 0x91, 0xce, 0xa9, 0x7c, 0x3c, 0x32, 0xad, 0x36 );
DEFINE_GUID ( WFP_SAMPLE_SUB_LAYER_GUID,              0xed6a516a, 0x36d1, 0x4881, 0xbc, 0xf0, 0xac, 0xeb, 0x4c,  0x4, 0xc2, 0x1c );


UINT32 fwpsCalloutID  = 0;
UINT32 AddCalloutID   = 0;
UINT64 FilterID       = 0;

NTSTATUS NotifyCallback ( FWPS_CALLOUT_NOTIFY_TYPE type, const GUID* filter_key, const FWPS_FILTER* filter ) 
{
	UNREFERENCED_PARAMETER ( type );
	UNREFERENCED_PARAMETER ( filter_key );
	UNREFERENCED_PARAMETER ( filter );
	return STATUS_SUCCESS;
}

VOID FlowDeleteCallback ( UINT16 layer_id, UINT32 callout_id, UINT64 flow_context ) 
{
	UNREFERENCED_PARAMETER ( layer_id );
	UNREFERENCED_PARAMETER ( callout_id );
	UNREFERENCED_PARAMETER ( flow_context );
	return;
}

VOID FilterCallback ( const FWPS_INCOMING_VALUE0* values, const FWPS_INCOMING_METADATA_VALUES0 metadata, const PVOID layer_data, const void* context, const FWPS_FILTER* filter, UINT64 flow_context, FWPS_CLASSIFY_OUT* classify_out ) 
{
	UNREFERENCED_PARAMETER ( values        );
	UNREFERENCED_PARAMETER ( metadata      );
	UNREFERENCED_PARAMETER ( context       );
	UNREFERENCED_PARAMETER ( flow_context  );
	UNREFERENCED_PARAMETER ( filter        );
	UNREFERENCED_PARAMETER ( layer_data    );
	UNREFERENCED_PARAMETER ( classify_out  );


	FWPS_STREAM_CALLOUT_IO_PACKET* packet = 0;
	FWPS_STREAM_DATA* stream_data         = 0;
	
	UCHAR string [ 201 ]                  = { 0 };
	SIZE_T lenght                         = 0;
	SIZE_T bytes                          = 0;

	KdPrint ( ( "<<<*************************** DATA ***************************>>>\r\n" ) );


	packet = ( FWPS_STREAM_CALLOUT_IO_PACKET* ) layer_data;
	
	// I made a bad typo!
	stream_data = packet->streamData;


	RtlZeroMemory ( classify_out, sizeof ( FWPS_CLASSIFY_OUT ) );

	if ( stream_data->flags & FWPS_STREAM_FLAG_RECEIVE ) 
	{
		lenght = stream_data->dataLength <= 200 ? stream_data->dataLength : 200;

		FwpsCopyStreamDataToBuffer ( stream_data, string, lenght, &bytes );

		KdPrint ( ( "data is %s\r\n", string ) );
	}

	packet->streamAction     = FWPS_STREAM_ACTION_NONE;
	classify_out->actionType = FWP_ACTION_PERMIT;

	if ( filter->flags & FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT ) 
	{
		classify_out->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
}


VOID DeinitializeWfp ( )
{
		if ( EngineHandle != NULL ) 
		{

		if ( FilterID != 0 ) 
		{
			FwpmFilterDeleteById    ( EngineHandle, FilterID );
			FwpmSubLayerDeleteByKey ( EngineHandle, &WFP_SAMPLE_SUB_LAYER_GUID );
		}

		if ( AddCalloutID != 0 ) 
		{
			FwpmCalloutDeleteById ( EngineHandle, AddCalloutID );
		}

		if ( fwpsCalloutID != 0 ) 
		{
			FwpsCalloutUnregisterById ( fwpsCalloutID );
		}
		FwpmEngineClose ( EngineHandle );
	}
}



NTSTATUS InitializeWfp ( PDEVICE_OBJECT DeviceObject )
{
    if ( !NT_SUCCESS ( FwpmEngineOpen ( 0, RPC_C_AUTHN_WINNT, 0, 0, &EngineHandle ) ) ) 
	{
		DeinitializeWfp ( );
		return STATUS_UNSUCCESSFUL;
	}
	 
	FWPS_CALLOUT fwpsCallout = { 0 };
	fwpsCallout.calloutKey   = WFP_SAMPLE_ESTABLISHED_CALLOUT_V4_GUID;
	fwpsCallout.flags        = 0;
	fwpsCallout.classifyFn   = ( FWPS_CALLOUT_CLASSIFY_FN1 ) FilterCallback;
	fwpsCallout.notifyFn     = ( FWPS_CALLOUT_NOTIFY_FN1   ) NotifyCallback;
	fwpsCallout.flowDeleteFn = ( FWPS_CALLOUT_FLOW_DELETE_NOTIFY_FN0 ) FlowDeleteCallback;

	if ( !NT_SUCCESS ( FwpsCalloutRegister ( DeviceObject, &fwpsCallout, &fwpsCalloutID ) ) )
	{
		DeinitializeWfp ( );
		return STATUS_UNSUCCESSFUL;
	}
	
	FWPM_CALLOUT fwpmCallout = { 0 };
	fwpmCallout.flags                   = 0;
	fwpmCallout.displayData.name        = L"EstablishedCalloutName";
	fwpmCallout.displayData.description = L"EstablishedCalloutName";
	fwpmCallout.calloutKey              = WFP_SAMPLE_ESTABLISHED_CALLOUT_V4_GUID;
	fwpmCallout.applicableLayer         = FWPM_LAYER_STREAM_V4;

	if ( !NT_SUCCESS ( FwpmCalloutAdd ( EngineHandle, &fwpmCallout, NULL, &AddCalloutID ) ) )
	{
		DeinitializeWfp ( );
		return STATUS_UNSUCCESSFUL;
	}
	
	FWPM_SUBLAYER sublayer = { 0 };

	sublayer.displayData.name        = L"Establishedsublayername";
	sublayer.displayData.description = L"Establishedsublayername";
	sublayer.subLayerKey             = WFP_SAMPLE_SUB_LAYER_GUID;
	sublayer.weight                  = 65500;

	if ( !NT_SUCCESS ( FwpmSubLayerAdd ( EngineHandle, &sublayer, NULL ) ) )
	{
		DeinitializeWfp ( );
		return STATUS_UNSUCCESSFUL;
	}
		
	FWPM_FILTER filter                    = { 0 };
	FWPM_FILTER_CONDITION condition [ 1 ] = { 0 };
	filter.displayData.name               = L"filterCalloutName";
	filter.displayData.description        = L"filterCalloutName";
	filter.layerKey                       = FWPM_LAYER_STREAM_V4;
	filter.subLayerKey                    = WFP_SAMPLE_SUB_LAYER_GUID;
	filter.weight.type                    = FWP_EMPTY;
	filter.numFilterConditions            = 1;
	filter.filterCondition                = condition;
	filter.action.type                    = FWP_ACTION_CALLOUT_TERMINATING;
	filter.action.calloutKey              = WFP_SAMPLE_ESTABLISHED_CALLOUT_V4_GUID;
	condition [ 0 ].fieldKey              = FWPM_CONDITION_IP_LOCAL_PORT;
	condition [ 0 ].matchType             = FWP_MATCH_LESS_OR_EQUAL;
	condition [ 0 ].conditionValue.type   = FWP_UINT16;
	condition [ 0 ].conditionValue.uint16 = 65000;

	if ( !NT_SUCCESS ( FwpmFilterAdd ( EngineHandle, &filter, NULL, &FilterID ) ) )
	{
		DeinitializeWfp ( );
		return STATUS_UNSUCCESSFUL;
	}
	
	return STATUS_SUCCESS;
}


NTSTATUS DriverEntry ( _In_ PDRIVER_OBJECT   DriverObject, _In_ PUNICODE_STRING  RegistryPath )
{
    UNREFERENCED_PARAMETER     ( RegistryPath );
    DriverObject->DriverUnload = DriverUnload;

    UNICODE_STRING WFPNetFilterSymbolicLinkName;
    RtlInitUnicodeString ( &WFPNetFilterSymbolicLinkName, L"\\DosDevices\\WFPNetFilter");
	
	UNICODE_STRING WFPNetFilterDriverName;
	RtlInitUnicodeString ( &WFPNetFilterDriverName, L"\\Device\\WFPNetFilter" );
    
    PDEVICE_OBJECT DeviceObject;
    NTSTATUS status = IoCreateDevice ( DriverObject,
									   0,
									   &WFPNetFilterDriverName,
									   FILE_DEVICE_UNKNOWN,
									   FILE_DEVICE_SECURE_OPEN,
									   FALSE,
									   &DeviceObject );

    if ( !NT_SUCCESS ( status ) ) 
	{
        KdPrint ( ( "Failed to create the device object (0x%08X)\n", status ) );
        return status;
    }

    status = IoCreateSymbolicLink ( &WFPNetFilterSymbolicLinkName, &WFPNetFilterDriverName );

    if ( !NT_SUCCESS ( status ) ) 
	{
        KdPrint        ( ( "Failed to create symbolic link (0x%08X)\n", status ) );
        IoDeleteDevice ( DeviceObject );
        return status;
    }

	status = InitializeWfp ( DeviceObject );
    if ( !NT_SUCCESS ( status ) ) 
	{
        KdPrint        ( ( "Failed to initialize WFP (0x%08X)\n", status ) );
        IoDeleteSymbolicLink ( &WFPNetFilterSymbolicLinkName );
		IoDeleteDevice ( DeviceObject );
        return status;
    }

    KdPrint ( ( "Skeleton Driver Loaded Successfully\n" ) );

    return STATUS_SUCCESS;
}

VOID DriverUnload ( _In_ PDRIVER_OBJECT DriverObject )
{
    UNREFERENCED_PARAMETER ( DriverObject );
	
	DeinitializeWfp ( );
	

    UNICODE_STRING WFPNetFilterSymbolicLinkName;
    RtlInitUnicodeString ( &WFPNetFilterSymbolicLinkName, L"\\DosDevices\\WFPNetFilter" );
    IoDeleteSymbolicLink ( &WFPNetFilterSymbolicLinkName );
	
	IoDeleteDevice       ( DriverObject->DeviceObject );
    KdPrint              ( ( "Skeleton Driver Unloaded\n" ) );
}


